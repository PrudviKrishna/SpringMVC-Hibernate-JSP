package springmvc.DAO;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import springmvc.model.*;


@Component
public class eventDAO {
	private static SessionFactory sessionFactory = createSessionFactory();

	private static SessionFactory createSessionFactory() {
		Configuration configuration = new Configuration();
    	configuration.addAnnotatedClass(Event.class);
//		configuration.addAnnotatedClass(Credentials.class);
		return configuration.buildSessionFactory(new StandardServiceRegistryBuilder().build());

	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}
