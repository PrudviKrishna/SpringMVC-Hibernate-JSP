package springmvc.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import springmvc.model.Event;

@Repository
public class eventDAOImpl {

	@Autowired
	private eventDAO sessionFactory;

//	public void setSessionFactory(SessionFactory sf) {
//		this.sessionFactory = sf;
//	}

	public Event addEvent(Event event) {
		System.out.println("DAO in add event");
		@SuppressWarnings("static-access")
		Session session = this.sessionFactory.getSessionFactory().openSession();
		session.saveOrUpdate(event);
		session.close();
		System.out.println("session persisted");
		return event;
	}

	public List<Event> getAllEvents() {
		Session session = this.sessionFactory.getSessionFactory().openSession();
		List<Event> eventsList = session.createQuery("from Event").list();
		System.out.println("DAO in get all events");
		eventsList.forEach(System.out::println);
		return eventsList;
	}

	public Event getEvent(int id) {
		Session session = this.sessionFactory.getSessionFactory().openSession();
		Event event = (Event) session.get(Event.class, new Integer(id));
		return event;
	}


	public void deleteEvent(int id) {
		Session session = this.sessionFactory.getSessionFactory().openSession();
		Event e = (Event) session.load(Event.class, new Integer(id));
		System.out.println("Delete method:" + e);
		if (null != e) {
			session.delete(e);
		}
		session.close();
	}

}
