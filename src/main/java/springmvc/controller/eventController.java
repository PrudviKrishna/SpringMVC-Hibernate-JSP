package springmvc.controller;

import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import springmvc.model.Event;



public interface eventController {

	    String addEvent(Event event);
		ModelAndView getEvents(Model model);
	    Event getEventById(int id);
	    String deleteEvent(int id);
	   

}
