package springmvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.*;

import springmvc.model.Event;
import springmvc.service.eventService;


@Controller
public class eventControllerImpl implements eventController {

	@Autowired
	private eventService service;

	
	@RequestMapping(value = "/getAllEvents", method = RequestMethod.GET)
	public ModelAndView getEvents(Model model) {

		List<Event> listOfEvents = service.getAllEvents();
		ModelAndView mv=new ModelAndView();
		mv.addObject("event", new Event());
		mv.addObject("listOfEvents", listOfEvents);
	//	listOfEvents.forEach(System.out::println);
		mv.setViewName("eventDetails");
		return mv;
	}

	@RequestMapping(value = "/addEvent", method = RequestMethod.POST)
	public String addEvent(@ModelAttribute("event") Event event) {
		System.out.println("Hello");
		System.out.println("Event" + event);
		if (event.getId() == 0) {
			service.addEvent(event);
		} 

		return "redirect:/getAllEvents";
	}

	@RequestMapping(value = "/getEvent/{id}", method = RequestMethod.GET)
	public Event getEventById(@PathVariable int id) {
		return service.getEvent(id);
	}

	@RequestMapping(value = "/deleteEvent/{id}", method = RequestMethod.GET)
	public String deleteEvent(@PathVariable("id") int id) {
		service.deleteEvent(id);
		return "redirect:/getAllEvents";
	}

	@RequestMapping(value = "/updateEvent/{id}", method = RequestMethod.GET)
	public String updateEvent(@PathVariable("id") int id, Model model) {
		System.out.println("Update event: ");

		model.addAttribute("event", this.service.getEvent(id));
		model.addAttribute("listOfEvents", this.service.getAllEvents());
		
		return "eventDetails";
	}

}
