package springmvc.service;

import java.util.List;

import springmvc.model.*;

public interface eventService {

	void addEvent(Event event);
	List<Event> getAllEvents();
    Event getEvent(int id);
    void deleteEvent(int id);
    
	
}
