package springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springmvc.DAO.eventDAOImpl;
import springmvc.model.*;

@Service
public class eventServiceImpl implements eventService {

	@Autowired
    private eventDAOImpl dao;
	
	@Override
	public void addEvent(Event event) {
		dao.addEvent(event);
		
	}

	@Override
	public List<Event> getAllEvents() {
		System.out.println("in service get all events");
		return dao.getAllEvents();
	}

	@Override
	public Event getEvent(int id) {
		return dao.getEvent(id);
	}

	@Override
	public void deleteEvent(int id) {
		dao.deleteEvent(id);
		
	}


	


	



	
}
